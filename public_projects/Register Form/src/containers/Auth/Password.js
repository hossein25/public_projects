import React, { Component } from "react";
import { Form, Input, Row, Col, Progress } from "antd";

class Password extends Component {
  render() {
    const { pass, getFieldDecorator } = this.props;
    return (
      <div>
        <Row>
          <Col>
            <Form.Item label="رمز عبور">
              {getFieldDecorator("password", {
                rules: [
                  {
                    required: true, message: "رمز عبور خود را وارد کنید"
                  }
                ]
              })(
                <Input.Password
                  placeholder="رمز عبور" onChange={this.props.handleInputPass}
                />
              )}
            </Form.Item>
            {pass.match(
              /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[#?!@$%^&*\-_]).{8,}$/
            ) ? (
              <Progress percent={100} />
            ) : pass.match(/^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z]).{8,}$/) ? (
              <Progress percent={70} status="active" />
            ) : pass.match(/^(?=.*?[0-9])(?=.*?[a-z]).{8,}$/) ? (
              <Progress percent={50} status="exception" />
            ) : pass.length >= 8 ? (
              <Progress strokeColor="orange" percent={35} status="exception" />
            ) : (
              pass.length < 8 && pass.length > 4 && (
                <Progress strokeColor="yellow" percent={20} status="exception" />
              )
            )}
          </Col>
        </Row>

        <Row style={{ marginTop: 10 }}>
          <Col>
            <Form.Item label="تایید رمز عبور">
              {getFieldDecorator("confirm_password", {
                rules: [
                  {
                    required: true, message: "رمز عبور خود را تایید کنید"
                  },
                  {
                    validator: this.props.handleConfirmPass
                  }
                ]
              })(<Input.Password placeholder="تایید رمز عبور" />)}
            </Form.Item>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Password;
