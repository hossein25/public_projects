import React, { Component } from "react";
import { Form, Input, Col } from "antd";

class InputEmail extends Component {
  render() {
    const { getFieldDecorator } = this.props;

    return (
      <div>
        <Col xs={{span:24}} sm={{span:24}} md={{span:24}} lg={{span:12}} span={12}>
          <Form.Item label="ایمیل">
            {getFieldDecorator("email", {
              validateTrigger: "onBlur",
              rules: [
                {
                  type: "email", message: "ایمیل صحیح نیست"
                },
                {
                  required: true, message: "ایمیل ضروری است"
                }
              ]
            })(<Input placeholder="ایمیل" />)}
          </Form.Item>
        </Col>
      </div>
    );
  }
}

export default InputEmail;
