import React, { Component } from "react";
import { Form, Input, Col } from "antd";

class InputNames extends Component {
  render() {
    const { getFieldDecorator } = this.props;

    const nameRules = {
      validateTrigger: "onBlur",
      rules: [
        {
          required: true, message: " ضروری است", whitesapce: true
        },
        {
          min: 2, message: " حداقل باید ۲ کاراکتر باشد"
        },
        {
          pattern: /^[ضصثقفغعهخحجچشسیبلاتنمکگظطزرذدپوژآ ]+$/,
          message: " فقط می تواند شامل حروف فارسی باشد"
        }
      ]
    };

    return (
      <div>
        <Col xs={{span:24}} sm={{span:24}} md={{span:24}} lg={{span:12}}>
          <Form.Item label="نام خانوادگی">
            {getFieldDecorator("last_name", nameRules)(
              <Input placeholder="نام خانوادگی" />
            )}
          </Form.Item>
        </Col>
        <Col xs={{span:24}} sm={{span:24}} md={{span:24}} lg={{span:12}}>
          <Form.Item label="نام">
            {getFieldDecorator("name", nameRules)(<Input placeholder="نام" />)}
          </Form.Item>
        </Col>
      </div>
    );
  }
}

export default InputNames;
