import React, { Component } from "react";
import { Form, Col } from "antd";
import DatePicker from "react-datepicker2";

class BirthdayPicker extends Component {
  render() {
    const { getFieldDecorator } = this.props;

    return (
      <div>
        <Col span={12}>
          <Form.Item label="تاریخ تولد">
            {getFieldDecorator("birthday", {
              rules: [
                {
                  type: "object", required: true, message: "تاریخ تولد ضروری است"
                }
              ]
            })(<DatePicker isGregorian={false} timePicker={false} />)}
          </Form.Item>
        </Col>
      </div>
    );
  }
}

export default BirthdayPicker;
