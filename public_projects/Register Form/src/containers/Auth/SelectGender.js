import React, { Component } from "react";
import { Form, Col,  Radio} from "antd";

class SelectGender extends Component {
  render() {
    const { getFieldDecorator } = this.props;

    return (
      <div>
        <Col span={12}>
          <Form.Item label="جنسیت" style={{ direction: "rtl" }}>
            {getFieldDecorator("gender")(
              <Radio.Group>
                <Radio value={"male"}>آقا</Radio>
                <Radio value={"female"}>خانم</Radio>
              </Radio.Group>
            )}
          </Form.Item>
        </Col>
      </div>
    );
  }
}

export default SelectGender;
