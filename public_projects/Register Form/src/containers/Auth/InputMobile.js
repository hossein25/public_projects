import React, { Component } from "react";
import { Form, Input, Col } from "antd";

class InputMobile extends Component {
  render() {
    const { getFieldDecorator } = this.props;

    return (
      <div>
        <Col xs={{span:24}} sm={{span:24}} md={{span:24}} lg={{span:12}} span={12}>
          <Form.Item label="شماره موبایل">
            {getFieldDecorator("phone", {
              validateTrigger: "onBlur",
              rules: [
                {
                  required: true, message: "فیلد شماره موبایل ضروری است"
                },
                {
                  len: 11, message: "شماره موبایل باید ۱۱ رقم باشد"
                }
              ]
            })(<Input type="number" placeholder="شماره موبایل" />)}
          </Form.Item>
        </Col>
      </div>
    );
  }
}

export default InputMobile;
