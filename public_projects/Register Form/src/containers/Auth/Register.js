import React, { Component } from "react";
import { Form, Row, Col, Button , message} from "antd";
import "./style.css";
import Password from "./Password";
import InputNames from "./InputNames";
import InputMobile from "./InputMobile";
import InputEmail from "./InputEmail";
import BirthdayPicker from "./BirthdayPicker";
import SelectGender from "./SelectGender";

class Register extends Component {
  state = {
    data: {
      pass: ""
    }
  };

  handleInputPass = event => {
    const pass = event.target.value;
    this.setState(state => ({
      data: {
        ...state.data,
        pass
      }
    }));
  };


  handleConfirmPass = (rule, value, callback) => {
    const confirmPass = value;
    if (this.state.data.pass !== confirmPass) {
      callback("رمز مشابه رمز اولیه نمی باشد");
    }
    callback();
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const value = {
          ...values,
          birthday: values["birthday"].format("jYYYY/jM/jD")
        };

        this.setState({ data1: value }, () =>{
          console.log(JSON.stringify(this.state.data1, null, 4))
          const value = JSON.stringify(this.state.data1, null, 4)
          this.success(value)
        });
      }
    });
  };

  success = (value) =>{
    message.info(value);

  }

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="formContainer">
        <Form onSubmit={this.handleSubmit}>
          <h1 className="headerForm">ثبت نام</h1>
          <img className="headerImg" src="./userAvatar.jpg" alt="user"/>

          <Row gutter={8} style={{ marginTop: 10 }}>
            <InputNames getFieldDecorator={getFieldDecorator} />
          </Row>

          <Row gutter={8}>
            <InputMobile getFieldDecorator={getFieldDecorator} />
            <InputEmail getFieldDecorator={getFieldDecorator} />
          </Row>

          <Row gutter={8}>
            <BirthdayPicker getFieldDecorator={getFieldDecorator} />
            <SelectGender getFieldDecorator={getFieldDecorator} />
          </Row>

          <Password
            handleInputPass={this.handleInputPass} getFieldDecorator={getFieldDecorator}
            handleConfirmPass={this.handleConfirmPass} pass={this.state.data.pass}
          />

          <Row>
            <Col span={8} offset={8}>
              <Form.Item>
                <Button className="submitButton" type="primary" htmlType="submit">
                  ثبت نام
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
	  </div>
    );
  }
}
const WrappedRegistrationForm = Form.create({ name: "register" })(Register);

export default WrappedRegistrationForm;
